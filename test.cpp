#include <vector>
#include <iostream>
#include <algorithm>
#include "schedulers.h"

typedef Process P;

const float AAE_TOLERANCE = 0.001;


bool assertAlmostEqual(float to_check, float value) {
	bool success = (to_check >= value - AAE_TOLERANCE ) && (to_check <= value + AAE_TOLERANCE);
	if (!success)
		std::cout << " [ FAIL ] assertAlmostEqual: Expected " << value << ", Got " << to_check << std::endl;
	return success;
}

bool make_test(const char* msg, float (*func)(std::vector<Process>), std::vector<Process> procs, float expected_output) {
	std::cout << msg << "..." << std::endl;
	return assertAlmostEqual(func(procs), expected_output);
}

bool make_test(const char* msg, float (*func)(const std::vector<Process>&), std::vector<Process> procs, float expected_output) {
	std::cout << msg << "..." << std::endl;
	return assertAlmostEqual(func(procs), expected_output);
}



int main() {
// Testing First Come First Output...
	make_test("Fist come First Output 1", &fcfo, {
			P(24), P(3), P(3)
	}, 17);

	make_test("Fist come First Output 2", &fcfo, {
			P(3), P(3), P(24)
	}, 3);

	make_test("First Come First Output 3", &fcfo, {
			P(10), P(29), P(3), P(7), P(12)
	}, 28);


// Testing Priority Queue...
	make_test("Priority Queue 3", &prio, {
			P(10, 2), P(29, 3), P(3, 4), P(7, 5), P(12, 1)
	}, 27.8);

// Testing Shortest Job First...
	make_test("Shortest Job First 1", &sjf, {
			P(24), P(3), P(3)
	}, 3);

	make_test("Shortest Job First 2", &sjf, {
			P(6), P(8), P(7), P(3)
	}, 7);

	make_test("Shortest Job First 3", &sjf, {
			P(10), P(29), P(3), P(7), P(12)
	}, 13);


// Testing Round Robin...
	make_test("Round Robin Test 1", &rr, {
			P(10), P(29), P(3), P(7), P(12)
	}, 23);

	make_test("Round Robin Test 2", &rr, {
			P(12), P(6), P(4)
	}, 12.);


	return 0;
}
