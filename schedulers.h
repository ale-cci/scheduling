#ifndef _SCHEDULERS_H_
#define _SCHEDULERS_H_

#include <vector>
#include <algorithm>

static int RROBIN_TIME=10;

struct Process {
	int exec_time, priority;

	Process(int execution_time, int process_priority=1) : exec_time(execution_time), priority(process_priority) {};
};


float sjf(std::vector<Process> procs) {
	int current_time=0,
		avg_time=0;

	std::sort(procs.begin(), procs.end(), [](const Process& first, const Process& second)->bool{
			return first.exec_time < second.exec_time;
	});

	for (const Process& p : procs) {
		avg_time += current_time;
		current_time += p.exec_time;
	}

	return float(avg_time) / procs.size();
}

float prio(std::vector<Process> procs) {
	int current_time=0,
		avg_time=0;

	std::sort(procs.begin(), procs.end(), [](const Process& first, const Process& second)->bool{
			return first.priority < second.priority;
	});

	for (const Process& p : procs) {
		avg_time += current_time;
		current_time += p.exec_time;
	}

	return float(avg_time) / procs.size();
}

float fcfo(const std::vector<Process>& procs) {
	int current_time = 0,
		avg_time =0;

	for (const Process& p : procs) {
		avg_time += current_time;
		current_time += p.exec_time;
	}

	return float(avg_time) / procs.size();
}


float rr(const std::vector<Process>& procs) {
	int current_time=0,
		avg_time=0,
		done_procs=0;
	int time_left[procs.size()];

	for (int i=0; i < procs.size(); ++i)
		time_left[i] = procs[i].exec_time;

	for (int i=0; done_procs < procs.size() ;++i) {
		i %= procs.size();
		// Process terminated
		if (time_left[i] == 0)
			continue;

		// Process still in queue
		if (time_left[i] <= RROBIN_TIME) {
			current_time += time_left[i];
			avg_time += (current_time - procs[i].exec_time);
			time_left[i] = 0;
			++done_procs;
		}
		else {
			current_time += RROBIN_TIME;
			time_left[i] -= RROBIN_TIME;
		}
	}
	return float(avg_time) / procs.size();
}

#endif //_SCHEDULERS_H_
