CC=g++
CFLAGS=-std=c++14 -g
BIN=run

default: main.cpp
	$(CC) main.cpp $(CFLAGS) -o $(BIN)


test: test.cpp
	$(CC) test.cpp $(CFLAGS) -o test

clean:
	rm $(BIN) test
