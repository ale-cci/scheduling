#include <vector>
#include <cstring>
#include <sstream>
#include <iostream>
#include "schedulers.h"

void usage(const char* argv0) {
	std::cout << "Usage: " << argv0 << " <option(s)> PROCESS_LIST" << std::endl
		<< std::endl
		<< "PROCESS_LIST is a list of comma separated value processes execution time"<< std::endl
		<< std::endl
		<< "Options: " << std::endl
		<< "\t-h, --help\t\tShow this help message" << std::endl
		<< "\t-v, --verbose\t\tDisplay full Scheduling algorithm names" << std::endl
		<< "\t-p, --priorities\tGive a CSV List of processes priorities" << std::endl
		<< "\t-r, --robin-time\tSpecify Round Robin time slice (default 10)" << std::endl
		<< std::endl;
}

int main(int argc, const char**argv) {
	std::vector<Process> lst_processes;

	if (argc == 1) {
		usage(argv[0]);
		exit(1);
	}

	bool verbose=false;
	std::string exec_times,
		priorities;

	// Command Line Argument Parsing
	for (int i=1; i < argc; ++i) {
		std::string arg = argv[i];

		if (arg == "-h" || arg == "--help"){
			usage(argv[0]);
			return 0;
		}
		else if (arg == "-v" || arg == "--verbose") {
			verbose = true;
		}
		else if (arg == "-r" || arg == "--robin-time") {
			// Consume 1 argument for round robin time
			++i;
			if ( i == argc) {
				usage(argv[0]);
				exit(1);
			}

			RROBIN_TIME=std::stoi(argv[i]);

			if (RROBIN_TIME <= 0) {
				std::cout << "Invalid Round Robin time" << std::endl;
				usage(argv[0]);
				exit(1);
			}

		}
		else if (arg == "-p" || arg == "--priorities") {
			// Consume 1 argument for properties
			++i;
			if ( i == argc) {
				usage (argv[0]);
				exit(1);
			}

			priorities=argv[i];
		}
		else {
			// No matches means execution times are given
			if (!lst_processes.empty()) {
				usage(argv[0]);
				exit(1);
			}

			exec_times=arg;
		}
	}

	std::string data;
	int proc_time=0,
		proc_prio=1;

	// Parsing CSV data
	std::istringstream etime_sstream = std::istringstream(exec_times),
		 prior_sstream = std::istringstream(priorities);

	while (std::getline(etime_sstream, data, ',')) {
		proc_time=stoi(data);

		if (std::getline(prior_sstream, data, ','))
			proc_prio=stoi(data);

		lst_processes.push_back(Process(proc_time, proc_prio));
	}

	if (lst_processes.empty()){
		usage(argv[0]);
		exit(1);
	}

	// The actual Algorithm
	std::cout << (verbose ? "First Come First Output" : "FCFO") << ": "<< fcfo(lst_processes) << std::endl;
	std::cout << (verbose ? "Round Robin" : "RR") << ": " << rr(lst_processes) << std::endl;
	std::cout << (verbose ? "Shortest Job First" : "SJF") << ": " << sjf(lst_processes) << std::endl;
	std::cout << (verbose ? "Priority Queue" : "PQ") << ": " << prio(lst_processes) << std::endl;

	return 0;
}
